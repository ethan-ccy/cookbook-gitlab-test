# cookbook-gitlab-test-cookbook

This cookbook configures an Ubuntu 14.04 machine to run the GitLab test suite
using GitLab CI.

## Registering a runner

After running this recipe on a node you still need to manually register the new
node with your GitLab CI coordinator. You can use the following interactive
command for that:

```
sudo /opt/gitlab-runner/bin/setup -C /home/gitlab-runner
sudo service gitlab-runner restart
```

## Supported Platforms

Ubuntu 14.04, maybe Ubuntu 12.04

## Usage without a Chef server

```
# Run as root
(
set -e
set -u
set -x

apt-get update

# Install the Chef client
curl -L https://www.chef.io/chef/install.sh | bash

# Download the cookbook-gitlab-test cookbook using Git and run its 'default' recipe
apt-get install -y git
chef_root=/tmp/cookbook-gitlab-test.$$
mkdir "$chef_root"
cd "$chef_root"
mkdir cookbooks
git clone https://gitlab.com/gitlab-org/cookbook-gitlab-test.git cookbooks/cookbook-gitlab-test
/opt/chef/bin/chef-client -z -r 'recipe[cookbook-gitlab-test::default]'
)
```

After this, the next step is to [register the runner](#register-a-runner) and
set up the [GitLab test
script](https://gitlab.com/gitlab-org/gitlab-ci/blob/master/doc/examples/build_script_gitlab_ce.md)
on your CI server.

## Attributes

```json
{
  "cookbook-gitlab-test": {
    "runner_deb": "download location for gitlab-runner package",
    "runner_deb_checksum": "SHA256 checksum for the package",
    "ruby_version": "2.1.5",
    "packages": [
      "libicu-dev",
      "other packages needed for the GitLab tests"
    ]
  }
}
```

## Usage

### cookbook-gitlab-test::default

Include `cookbook-gitlab-test` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[cookbook-gitlab-test::default]"
  ]
}
```

## License and Authors

Author:: Jacob Vosmaer (jacob@gitlab.com)
