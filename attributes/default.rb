default['cookbook-gitlab-test']['runner_deb'] = 'https://s3-eu-west-1.amazonaws.com/downloads-packages/ubuntu-14.04/gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb'
default['cookbook-gitlab-test']['runner_deb_checksum'] = 'd226a4ff384b55e7535a913b308d763b316b8a3fa8f49f09c91d7fd3eb65f238'
default['cookbook-gitlab-test']['ruby_version'] = '2.1.6'
default['cookbook-gitlab-test']['ruby_build_revision'] = 'e9d25d0f615d2dc2e6b7b32562d6a4eee9e9889e' # random revision which knows how to build 2.1.6
# Packages needed to run the GitLab test suite
default['cookbook-gitlab-test']['packages'] = %w{libicu-dev nodejs fontconfig cmake libkrb5-dev redis-server git-annex}
