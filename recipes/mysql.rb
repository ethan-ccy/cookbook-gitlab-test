%w{mysql-server mysql-client libmysqlclient-dev}.each do |pkg|
  package pkg
end

execute 'create MySQL user "runner"' do
  command %Q{
mysql -u root <<EOF
CREATE USER 'runner'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON * . * TO 'runner'@'localhost';
FLUSH PRIVILEGES;
EOF
  }
  not_if "mysql -u root -e 'USE mysql; SELECT user FROM user' | grep -w runner"
end
