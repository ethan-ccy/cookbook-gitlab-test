#
# Cookbook Name:: cookbook-gitlab-test
# Recipe:: default
#
# Copyright (C) 2015 GitLab B.V.
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'cookbook-gitlab-test::runner'
include_recipe 'cookbook-gitlab-test::ruby'
include_recipe 'cookbook-gitlab-test::mysql'
include_recipe 'cookbook-gitlab-test::phantomjs'

node['cookbook-gitlab-test']['packages'].each do |pkg|
  package pkg
end
