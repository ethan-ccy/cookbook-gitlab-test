phantomjs_tarball = '/home/gitlab-runner/phantomjs.tar.bz2'
remote_file phantomjs_tarball do
  source 'http://phantomjs.googlecode.com/files/phantomjs-1.8.1-linux-x86_64.tar.bz2'
  checksum '92c520204ef3abbe70b3696347017c66eb39dd80e6372098e3faf15ee92165b0'
end

bash 'install phantomjs' do
  code <<-EOH
  set -e # exit on failure
  set -x # echo commands for debugging
  cd /home/gitlab-runner
  tar -xjf #{phantomjs_tarball}
  rm -rf phantomjs
  mv phantomjs-1.8.1-linux-x86_64 phantomjs
  mkdir -p bin
  ln -s /home/gitlab-runner/phantomjs/bin/phantomjs /home/gitlab-runner/bin/
  EOH

  user 'gitlab-runner'
  group 'gitlab-runner'
  not_if "/home/gitlab-runner/bin/phantomjs --version | grep '^1\.8\.1$'"
end
