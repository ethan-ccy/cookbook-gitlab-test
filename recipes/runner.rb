omnibus_runner_deb = '/var/cache/omnibus-gitlab-runner.deb'
remote_file omnibus_runner_deb do
  source node['cookbook-gitlab-test']['runner_deb']
  checksum node['cookbook-gitlab-test']['runner_deb_checksum']
end

dpkg_package 'gitlab-runner' do
  source omnibus_runner_deb
end

user 'gitlab-runner' do
  shell '/bin/false'
  system true
  supports :manage_home => true
end

directory '/home/gitlab-runner' do
  recursive true
  owner 'gitlab-runner'
  group 'gitlab-runner'
end

remote_file '/etc/init/gitlab-runner.conf' do
  source 'file:///opt/gitlab-runner/doc/install/upstart/gitlab-runner.conf'
  notifies :run, 'execute[service gitlab-runner start]'
end

execute 'service gitlab-runner start' do
  action :nothing
end
