# Install Ruby from source on Ubuntu 14.04

package 'git' # Needed for everything

# Packages for compiling Ruby
%w{
g++ gcc make libc6-dev libreadline6-dev zlib1g-dev
libssl-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev
libncurses5-dev automake libtool bison pkg-config libffi-dev
}.each do |pkg|
  package pkg
end

ruby_build_install_dir = '/home/gitlab-runner/ruby-build'

git ruby_build_install_dir do
  repository 'https://github.com/sstephenson/ruby-build.git'
  revision node['cookbook-gitlab-test']['ruby_build_revision']
  notifies :run, 'execute[install ruby-build]', :immediately
  user 'gitlab-runner'
  group 'gitlab-runner'
end

execute 'install ruby-build' do
  command './install.sh'
  user 'gitlab-runner'
  group 'gitlab-runner'
  cwd ruby_build_install_dir
  env 'PREFIX' => '/home/gitlab-runner'
  action :nothing
end

ruby_version = node['cookbook-gitlab-test']['ruby_version']
execute "/home/gitlab-runner/bin/ruby-build #{ruby_version} /home/gitlab-runner" do
  user 'gitlab-runner'
  group 'gitlab-runner'
  not_if "/home/gitlab-runner/bin/ruby --version | grep 'ruby #{ruby_version}'"
end

execute '/home/gitlab-runner/bin/gem install bundler --no-ri --no-rdoc' do
  user 'gitlab-runner'
  group 'gitlab-runner'
  creates '/home/gitlab-runner/bin/bundle'
end
